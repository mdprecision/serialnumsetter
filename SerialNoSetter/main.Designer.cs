﻿namespace SerialNoSetter
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
            this.textBoxSerialNumber = new System.Windows.Forms.TextBox();
            this.buttonProg = new System.Windows.Forms.Button();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.serialPortWand = new System.IO.Ports.SerialPort(this.components);
            this.listBoxReadBack = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonTare = new System.Windows.Forms.Button();
            this.comboBoxTest = new System.Windows.Forms.ComboBox();
            this.buttonCheck = new System.Windows.Forms.Button();
            this.buttonContinuousRead = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxComPort = new System.Windows.Forms.TextBox();
            this.textBoxUID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSN = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.textBoxCommandLine = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fIleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLogToFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableCommandLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxSerialNumber
            // 
            this.textBoxSerialNumber.Location = new System.Drawing.Point(168, 72);
            this.textBoxSerialNumber.Name = "textBoxSerialNumber";
            this.textBoxSerialNumber.Size = new System.Drawing.Size(121, 23);
            this.textBoxSerialNumber.TabIndex = 1;
            this.textBoxSerialNumber.Text = "SN9999";
            this.textBoxSerialNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonProg
            // 
            this.buttonProg.Location = new System.Drawing.Point(168, 24);
            this.buttonProg.Name = "buttonProg";
            this.buttonProg.Size = new System.Drawing.Size(121, 46);
            this.buttonProg.TabIndex = 2;
            this.buttonProg.Text = "Serial Number:";
            this.buttonProg.UseVisualStyleBackColor = true;
            this.buttonProg.Click += new System.EventHandler(this.buttonProg_Click);
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(8, 8);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(159, 46);
            this.buttonConnect.TabIndex = 4;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // listBoxReadBack
            // 
            this.listBoxReadBack.BackColor = System.Drawing.Color.Black;
            this.listBoxReadBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxReadBack.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxReadBack.ForeColor = System.Drawing.Color.White;
            this.listBoxReadBack.FormattingEnabled = true;
            this.listBoxReadBack.Location = new System.Drawing.Point(3, 212);
            this.listBoxReadBack.Name = "listBoxReadBack";
            this.listBoxReadBack.Size = new System.Drawing.Size(833, 305);
            this.listBoxReadBack.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 74);
            this.button1.TabIndex = 7;
            this.button1.Text = "Testing SN";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 27);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(3);
            this.panel1.Size = new System.Drawing.Size(833, 185);
            this.panel1.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(184, 3);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(3);
            this.panel2.Size = new System.Drawing.Size(646, 179);
            this.panel2.TabIndex = 18;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Enabled = false;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(20, 6);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(640, 173);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.textBoxSerialNumber);
            this.tabPage1.Controls.Add(this.buttonProg);
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(632, 139);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Production";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.buttonContinuousRead);
            this.tabPage2.Location = new System.Drawing.Point(4, 30);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(632, 139);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sensor Tester";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonTare);
            this.groupBox1.Controls.Add(this.comboBoxTest);
            this.groupBox1.Controls.Add(this.buttonCheck);
            this.groupBox1.Location = new System.Drawing.Point(184, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 112);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sensor";
            // 
            // buttonTare
            // 
            this.buttonTare.BackColor = System.Drawing.Color.White;
            this.buttonTare.Location = new System.Drawing.Point(16, 24);
            this.buttonTare.Name = "buttonTare";
            this.buttonTare.Size = new System.Drawing.Size(121, 74);
            this.buttonTare.TabIndex = 9;
            this.buttonTare.Text = "TARE";
            this.buttonTare.UseVisualStyleBackColor = false;
            this.buttonTare.Click += new System.EventHandler(this.buttonTare_Click);
            // 
            // comboBoxTest
            // 
            this.comboBoxTest.FormattingEnabled = true;
            this.comboBoxTest.Items.AddRange(new object[] {
            "Slot A",
            "Slot B"});
            this.comboBoxTest.Location = new System.Drawing.Point(152, 24);
            this.comboBoxTest.Name = "comboBoxTest";
            this.comboBoxTest.Size = new System.Drawing.Size(120, 23);
            this.comboBoxTest.TabIndex = 11;
            // 
            // buttonCheck
            // 
            this.buttonCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonCheck.Location = new System.Drawing.Point(152, 56);
            this.buttonCheck.Name = "buttonCheck";
            this.buttonCheck.Size = new System.Drawing.Size(121, 40);
            this.buttonCheck.TabIndex = 10;
            this.buttonCheck.Text = "Check";
            this.buttonCheck.UseVisualStyleBackColor = false;
            this.buttonCheck.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // buttonContinuousRead
            // 
            this.buttonContinuousRead.BackColor = System.Drawing.Color.Yellow;
            this.buttonContinuousRead.Location = new System.Drawing.Point(32, 32);
            this.buttonContinuousRead.Name = "buttonContinuousRead";
            this.buttonContinuousRead.Size = new System.Drawing.Size(121, 74);
            this.buttonContinuousRead.TabIndex = 8;
            this.buttonContinuousRead.Text = "Read Magnetometers";
            this.buttonContinuousRead.UseVisualStyleBackColor = false;
            this.buttonContinuousRead.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonConnect);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.textBoxComPort);
            this.panel3.Controls.Add(this.textBoxUID);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.textBoxSN);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(3);
            this.panel3.Size = new System.Drawing.Size(181, 179);
            this.panel3.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 25);
            this.label3.TabIndex = 15;
            this.label3.Text = "SN:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 25);
            this.label2.TabIndex = 13;
            this.label2.Text = "Device:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxComPort
            // 
            this.textBoxComPort.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxComPort.Location = new System.Drawing.Point(64, 73);
            this.textBoxComPort.Name = "textBoxComPort";
            this.textBoxComPort.ReadOnly = true;
            this.textBoxComPort.Size = new System.Drawing.Size(103, 23);
            this.textBoxComPort.TabIndex = 10;
            this.textBoxComPort.Text = "?";
            this.textBoxComPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxUID
            // 
            this.textBoxUID.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUID.Location = new System.Drawing.Point(64, 111);
            this.textBoxUID.Name = "textBoxUID";
            this.textBoxUID.ReadOnly = true;
            this.textBoxUID.Size = new System.Drawing.Size(103, 23);
            this.textBoxUID.TabIndex = 12;
            this.textBoxUID.Text = "?";
            this.textBoxUID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 25);
            this.label1.TabIndex = 11;
            this.label1.Text = "Port:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxSN
            // 
            this.textBoxSN.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSN.Location = new System.Drawing.Point(64, 139);
            this.textBoxSN.Name = "textBoxSN";
            this.textBoxSN.ReadOnly = true;
            this.textBoxSN.Size = new System.Drawing.Size(103, 23);
            this.textBoxSN.TabIndex = 14;
            this.textBoxSN.Text = "?";
            this.textBoxSN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // textBoxCommandLine
            // 
            this.textBoxCommandLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxCommandLine.Enabled = false;
            this.textBoxCommandLine.Location = new System.Drawing.Point(3, 517);
            this.textBoxCommandLine.Name = "textBoxCommandLine";
            this.textBoxCommandLine.Size = new System.Drawing.Size(833, 23);
            this.textBoxCommandLine.TabIndex = 10;
            this.textBoxCommandLine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fIleToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(833, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fIleToolStripMenuItem
            // 
            this.fIleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportLogToFileToolStripMenuItem});
            this.fIleToolStripMenuItem.Name = "fIleToolStripMenuItem";
            this.fIleToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fIleToolStripMenuItem.Text = "File";
            // 
            // exportLogToFileToolStripMenuItem
            // 
            this.exportLogToFileToolStripMenuItem.Name = "exportLogToFileToolStripMenuItem";
            this.exportLogToFileToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.exportLogToFileToolStripMenuItem.Text = "Export log to file ...";
            this.exportLogToFileToolStripMenuItem.Click += new System.EventHandler(this.exportLogToFileToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enableCommandLineToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // enableCommandLineToolStripMenuItem
            // 
            this.enableCommandLineToolStripMenuItem.CheckOnClick = true;
            this.enableCommandLineToolStripMenuItem.Name = "enableCommandLineToolStripMenuItem";
            this.enableCommandLineToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.enableCommandLineToolStripMenuItem.Text = "Enable command line";
            this.enableCommandLineToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.enableCommandLineToolStripMenuItem_CheckStateChanged);
            this.enableCommandLineToolStripMenuItem.Click += new System.EventHandler(this.enableCommandLineToolStripMenuItem_Click);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 543);
            this.Controls.Add(this.listBoxReadBack);
            this.Controls.Add(this.textBoxCommandLine);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "main";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Text = "MOLLI Wand Production Tool (PT-002) Version 1.0";
            this.Load += new System.EventHandler(this.main_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxSerialNumber;
        private System.Windows.Forms.Button buttonProg;
        private System.Windows.Forms.Button buttonConnect;
        private System.IO.Ports.SerialPort serialPortWand;
        private System.Windows.Forms.ListBox listBoxReadBack;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonContinuousRead;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxUID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxComPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSN;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxCommandLine;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fIleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportLogToFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableCommandLineToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonTare;
        private System.Windows.Forms.ComboBox comboBoxTest;
        private System.Windows.Forms.Button buttonCheck;
    }
}

