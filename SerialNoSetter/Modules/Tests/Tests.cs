﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerialNoSetter
{
    public class ClsTests
    {
        public ClsTests() { }

        public void SetDefault()
        {
            Positions.Add(new ClsTestPosition("Slot A", "Please put in slot A", new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
            Positions.Add(new ClsTestPosition("Slot B", "Please put in slot B", new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
            Positions.Add(new ClsTestPosition("Slot C", "Please put in slot C", new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
            Positions.Add(new ClsTestPosition("Slot D", "Please put in slot D", new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
        }

        /// <summary>
        /// 
        /// </summary>
        public int NumOfReadingsForAverage { get; set; } = 10;

        /// <summary>
        /// Various tests position
        /// </summary>
        public List<ClsTestPosition> Positions { get; set; } = new List<ClsTestPosition>();
    }
}
