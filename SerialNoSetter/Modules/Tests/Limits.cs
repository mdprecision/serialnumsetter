﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SerialNoSetter
{
    [XmlType("Sensor")]
    public class ClsLimits
    {
        public ClsLimits() { }

        public ClsLimits(int index, double nomimal, double range)
        {
            Index = index;
            Nominal = nomimal;
            Range = range;
        }

        /// <summary>
        /// Sensor index
        /// </summary>
        [XmlAttribute]
        public double Index { get; set; } = 0;

        /// <summary>
        /// Nominal value
        /// </summary>
        [XmlAttribute]
        public double Nominal { get; set; } = 0;

        /// <summary>
        /// Pass/fail range
        /// </summary>
        [XmlAttribute]
        public double Range { get; set; } = 0;

        /// <summary>
        /// Lower range
        /// </summary>
        public double Lower => Nominal - Range;

        /// <summary>
        /// Uppper range
        /// </summary>
        public double Upper => Nominal + Range;

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{Nominal - Range} < x < {Nominal + Range}";

        /// <summary>
        /// Check if the value is within the limit
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool CheckBound(double value)
        {
            if (value > Upper)
            { return true; }
            else if (value < Lower)
            { return true; }
            else
            { return false; }
        }
    }
}
