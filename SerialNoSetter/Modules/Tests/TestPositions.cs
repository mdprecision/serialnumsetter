﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SerialNoSetter
{
    [XmlType("TestPosition")]
    public class ClsTestPosition
    {
        public ClsTestPosition() { }

        public ClsTestPosition(string p, string m, double[] Nominals)
        {
            PullDownMessage = p;
            MessageToUser = m;

            Limits.Clear();
            int index = 0;
            foreach (var nominal in Nominals)
            { Limits.Add(new ClsLimits(index++, nominal, 100)); }
        }

        /// <summary>
        /// Message to the user
        /// </summary>
        public string MessageToUser { get; set; } = "";

        /// <summary>
        /// Pull down message
        /// </summary>
        public string PullDownMessage { get; set; } = "";

        /// <summary>
        /// Limits for the sensors
        /// </summary>
        public List<ClsLimits> Limits { get; set; } = new List<ClsLimits>();

        public override string ToString()
        {
            List<string> msg = new List<string>();
            foreach (var item in Limits)
            { msg.Add(item.Nominal + " ±" + item.Range); }
            return string.Join(", ", msg);
        }
    }
}
