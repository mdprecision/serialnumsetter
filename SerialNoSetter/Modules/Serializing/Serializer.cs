﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Serialization;

namespace SerialNoSetter
{
    /// <summary>
    /// save/load object to a file - either CONFIG or COMMON folder
    /// </summary>
    public static class SerializeToXml
    {
        /// <summary>
        /// Read file and deserialize
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static T Read<T>(String FileName = "") where T : new()
        {

            //>	create a blank one
            T returnData = new T();

            try
            {
                if (String.IsNullOrEmpty(FileName) == true)
                { FileName = ObjectAsFileName<T>(); }

                //>	read if the file exists
                if (File.Exists(FileName))
                {
                    using (StreamReader file = new StreamReader(FileName))
                    { returnData = (T)new XmlSerializer(typeof(T)).Deserialize(file); }
                }
            }
            catch (Exception ex) { throw new Exception("Unable to deserialize file", ex); }

            //>	return te base class
            return returnData;
        }

        public static T Read<T>(T obj, String FileName = "") where T : new()
        {

            //>	create a blank one
            T returnData = new T();

            try
            {
                if (String.IsNullOrEmpty(FileName) == true)
                { FileName = ObjectAsFileName<T>(); }

                //>	read if the file exists
                if (File.Exists(FileName))
                {
                    using (StreamReader file = new StreamReader(FileName))
                    { returnData = (T)new XmlSerializer(typeof(T)).Deserialize(file); }
                }
            }
            catch (Exception ex) { throw new Exception("Unable to deserialize file", ex); }


            //>	return te base class
            return returnData;

        }

        static String GenericFilename = "generic.xml";


        static String ObjectAsFileName<T>()
        {
            String fileName = BaseFolderPaths.Configuration + GenericFilename;

            try
            {
                XmlRootAttribute theAttrib = Attribute.GetCustomAttribute(typeof(T), typeof(XmlRootAttribute)) as XmlRootAttribute;
                fileName = BaseFolderPaths.Configuration + theAttrib.Namespace + ".xml";
            }
            catch (Exception ex) { throw new Exception("Unable to get file name", ex); }

            //>	return fileName
            return fileName;
        }

        static String ObjectAsFileName(object obj)
        {
            String fileName = BaseFolderPaths.Configuration + GenericFilename;

            try
            {
                XmlRootAttribute theAttrib = Attribute.GetCustomAttribute(obj.GetType(), typeof(XmlRootAttribute)) as XmlRootAttribute;
                fileName = BaseFolderPaths.Configuration + theAttrib.Namespace + ".xml";
            }
            catch (Exception ex) { throw new Exception("Unable to get file name", ex); }

            //>	return fileName
            return fileName;
        }

        /// <summary>
        /// Write object to an XML file
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="FileName"></param>
        public static void Write<T>(this T obj, String FileName = "")
        {
            //; VMS-743 Sensors got stuck in an endless loop of connecting / disconnecting

            try
            {
                //>	if the filename is not specified
                if (String.IsNullOrEmpty(FileName) == true)
                { FileName = ObjectAsFileName(obj); }

                //>	write file
                using (StreamWriter file = new StreamWriter(FileName))
                { new XmlSerializer(obj.GetType()).Serialize(file, obj); }
            }
            catch (Exception ex) { throw new Exception("Unable to serialize object", ex); }


        }

        /// <summary>
        /// Write object to an XML file
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="FileName"></param>
        public static void Write(this object obj, String FileName = "")
        {
            //; VMS-743 Sensors got stuck in an endless loop of connecting / disconnecting

            try
            {
                //>	if the filename is not specified
                if (String.IsNullOrEmpty(FileName) == true)
                { FileName = ObjectAsFileName(obj); }

                //>	write file
                using (StreamWriter file = new StreamWriter(FileName))
                { new XmlSerializer(obj.GetType()).Serialize(file, obj); }
            }
            catch (Exception ex) { throw new Exception("Unable to serialize object", ex); }


        }
    }
}
