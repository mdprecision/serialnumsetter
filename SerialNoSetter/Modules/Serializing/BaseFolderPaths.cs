﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SerialNoSetter
{
    public static class BaseFolderPaths
    {
        //>	private local variables
        static String rootName = "MolliMfgTester\\";

        /// <summary>
        /// Path location of MyDocuments folder
        /// </summary>
        public static String MyDocumentsPath => Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\";

        static Dictionary<String, String> dictionarySubFolder = new Dictionary<string, String>();

        /// <summary>
        /// Folder path for the application files
        /// </summary>
        public static String Application { get { return SubFolder("Application"); } }

        /// <summary>
        /// XML configuration folder
        /// </summary>
        public static String Configuration { get { return SubFolder("Configuration"); } }

        /// <summary>
        /// Root folder path, in My Documents folder
        /// </summary>
        public static String Root { get { return MyDocumentsPath + rootName; } }

        /// <summary>
        /// Set the root name
        /// </summary>
        public static void SetRootPath(String FolderName)
        {
            //>	ensure name ends with backslash
            String potentialFolderName = FolderName;
            if (potentialFolderName.EndsWith("\\") == false)
            { potentialFolderName += "\\"; }

            //>	build potential folder path
            String potentailFolderPath = MyDocumentsPath + potentialFolderName;

            //>	assign the new path
            EnsureDirectoryExists(potentailFolderPath);
            rootName = potentialFolderName;
            dictionarySubFolder = new Dictionary<string, String>();
        }

        /// <summary>
        /// Ensure a directory exists
        /// </summary>
        /// <param name="directoryPath">String</param>
        /// <returns></returns>
        public static bool EnsureDirectoryExists(String directoryPath)
        {
            if (Directory.Exists(directoryPath) == false)
            { Directory.CreateDirectory(directoryPath); }
            return true;
        }

        /// <summary>
        /// Ensure a directory exists
        /// </summary>
        /// <param name="directoryPath">String</param>
        /// <returns></returns>
        public static bool AsEnsureDirectoryExists(this String directoryPath)
        { return EnsureDirectoryExists(directoryPath); }

        /// <summary>
        /// Access subfolder in the Root folder
        /// </summary>
        /// <param name="FolderName">String</param>
        /// <returns></returns>
        public static String SubFolder(String FolderName)
        {
            //>	Check if we have the full name for the path
            if (dictionarySubFolder.ContainsKey(FolderName) == true)
            {
                //> if we have accessed this folder before, just call it from dictionary
                return dictionarySubFolder[FolderName];
            }
            else
            {
                String potentailFolderPath = String.Empty;

                //>	ensure name ends with slash
                if (FolderName.EndsWith("\\") == false)
                { potentailFolderPath = Root + FolderName + "\\"; }

                //>	assign the new path
                EnsureDirectoryExists(potentailFolderPath);

                //>	add to dictionary
                dictionarySubFolder.Add(FolderName, potentailFolderPath);

                //>	return folder path
                return potentailFolderPath;
            }
        }
    }
}
