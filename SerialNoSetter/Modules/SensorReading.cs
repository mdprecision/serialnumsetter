﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SerialNoSetter
{
    public class ClsSensorReading
    {
        public List<double> Values = new List<double>();

        public ClsSensorReading()
        {
            for (int i = 0; i < 12; i++)
            {
                Values.Add(0);
            }

        }

        public void OutputToConsole()
        {
            Console.WriteLine($"Values: {String.Join(", ", Values)}");
        }

        public override string ToString() => $"{String.Join(", ", Values.Select(u => u.ToString("0.0")))}";

        /// <summary>
        /// Parse a sensor reading from a line of data
        /// </summary>
        /// <param name="oneLineOfData"></param>
        public ClsSensorReading(string oneLineOfData)
        {
            var tabbedData = oneLineOfData.Split('\t');
            if (tabbedData.Length == 14)
            {
                var values = tabbedData.Select(theString =>
                {
                    // try to parse the double from the string
                    if (double.TryParse(theString, out double value))
                    { return value; }
                    else
                    { return 0; }
                }).ToList();

                // MOLLI formate has unused data in the first and last position
                values.RemoveAt(13);
                values.RemoveAt(0);

                // assign to the class variable
                Values = values;

                // debug
                OutputToConsole();
            }
        }

        public void Set(ClsSensorReading sensorReading)
        {
            // check if the two arrays have the same size
            if (sensorReading.Values.Count != Values.Count)
            { throw new ArgumentException("Array size does not match!"); }

            for (int i = 0; i < Values.Count; i++)
            { Values[i] = sensorReading.Values[i]; }
        }

        public void Add(ClsSensorReading sensorReading)
        {
            // check if the two arrays have the same size
            if (sensorReading.Values.Count != Values.Count)
            { throw new ArgumentException("Array size does not match!"); }

            for (int i = 0; i < Values.Count; i++)
            { Values[i] += sensorReading.Values[i]; }
        }

        public void Divide(double denominator)
        {
            // check if the two arrays have the same size
            if (denominator == 0)
            { throw new InvalidDataException("Array size does not match!"); }

            for (int i = 0; i < Values.Count; i++)
            { Values[i] /= denominator; }
        }

        public static Func<IEnumerable<string>> HowToGetData;

        public static ClsSensorReading GetAverage(int numOfAverages)
        {
            var Count = 0;
            var avgSensor = new ClsSensorReading();
            for (int i = 0; i < numOfAverages; i++)
            {
                var bufferedData = HowToGetData?.Invoke() ?? new List<string>();

                foreach (var oneLineOfData in bufferedData)
                {
                    avgSensor.Add(new ClsSensorReading(oneLineOfData));
                    Count++;
                }

            }

            avgSensor.Divide(Count);
            avgSensor.OutputToConsole();

            return avgSensor;
        }

        public void Subtract(ClsSensorReading tareValue)
        {
            // check if the two arrays have the same size
            if (tareValue.Values.Count != Values.Count)
            { throw new ArgumentException("Array size does not match!"); }

            for (int i = 0; i < Values.Count; i++)
            { Values[i] -= tareValue.Values[i]; }
        }

        /// <summary>
        /// Check all the sensors
        /// If all the sensors are OK, return -1
        /// Else return the index of the sensor that failed
        /// </summary>
        /// <param name="limits"></param>
        /// <returns></returns>
        public int CheckAllSensors(List<ClsLimits> limits)
        {
            // check if the two arrays have the same size
            if (limits.Count != Values.Count)
            { throw new ArgumentException("Array size does not match!"); }

            for (int i = 0; i < Values.Count; i++)
            {
                // if any sensor reading is out of bounds, return a true
                if (limits[i].CheckBound(Values[i]))
                { return i; }
            }

            // we made it this far, no sensor was out of bounds, return false
            return -1;
        }
    }
}
