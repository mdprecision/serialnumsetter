﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SerialNoSetter
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
        }

        private void main_Load(object sender, EventArgs e)
        {
            ClsSensorReading.HowToGetData = Command_I;

            var filenmae = BaseFolderPaths.Configuration + "TestLimits.xml";

            if (File.Exists(filenmae))
            { Tests = SerializeToXml.Read<ClsTests>(filenmae); }
            else
            { Tests.SetDefault(); }

            Tests.Write(filenmae);

            #region Set up the combo box items
            comboBoxTest.Items.Clear();
            foreach (var item in Tests.Positions)
            { comboBoxTest.Items.Add(item.PullDownMessage); }

            if (comboBoxTest.Items.Count > 0)
            { comboBoxTest.SelectedIndex = 0; }
            #endregion



            serialPortWand.NewLine = "\r";
            serialPortWand.DtrEnable = true;

            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(500);
                buttonConnect_Click("USB", null);
            });
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            // invoke on main thread
            this.Invoke(new Action(() =>
            {
                // if we are reconnecting to a sensor, assume the tare is not valid
                TareWasDone = false;

                var names = SerialPort.GetPortNames();
                foreach (var item in names)
                {
                    try
                    {
                        DisconnectAll();

                        textBoxComPort.Text = item;
                        serialPortWand.PortName = item;
                        serialPortWand.Open();

                        serialPortWand.Write("xx");
                        var stopWdt = ReadBackFromPort();
                        if (stopWdt.Any(u => u.ToLower().Contains("watchdog")))
                        {
                            Command_F();
                            break;
                        }
                    }
                    catch (Exception ex)
                    { writeToScreen(ex.ToString()); }
                }
            }));
        }

        private void Command_F()
        {
            try
            {
                serialPortWand.Write("f");
                var identity = ReadBackFromPort();

                if (!identity.Any())
                { return; }

                // we found the ID
                var idPart = identity.FirstOrDefault(u => u.ToLower().Contains("uid")).Split(':');
                if (idPart.Length == 2)
                { textBoxUID.Text = idPart[1].Trim(); }

                // we found the SN
                var snPart = identity.FirstOrDefault(u => u.ToLower().Contains("s/n")).Split(':');
                if (snPart.Length == 2)
                { textBoxSN.Text = snPart[1].Trim(); }
            }
            catch (Exception ex)
            { writeToScreen(ex.ToString()); }
        }

        private IEnumerable<string> Command_I()
        {
            if (serialPortWand.IsOpen)
            {
                serialPortWand.Write("i");
                return ReadBackFromPort(50);
            }
            else
            { return null; }
        }



        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        protected override void WndProc(ref Message m)
        {
            // Listen for operating system messages.
            var message = m.Msg;
            string WParam;
            switch (message)
            {
                case 537:

                    WParam = m.WParam.ToString();

                    // A device or piece of media has been removed.
                    if (WParam.Contains("32772"))
                    { DisconnectAll(); }
                    // A device or piece of media has been added.
                    else if (WParam.Contains("32768"))
                    { buttonConnect_Click("USB", null); }

                    break;
                default:
                    break;
            }



            base.WndProc(ref m);
        }

        private void DisconnectAll()
        {
            try
            {
                continueRead = false;

                if (serialPortWand.IsOpen)
                { serialPortWand.Close(); }

                textBoxUID.Text = "?";
                textBoxSN.Text = "?";
                textBoxComPort.Text = "?";
            }
            catch (Exception ex)
            { writeToScreen(ex.ToString()); }
        }

        private void buttonProg_Click(object sender, EventArgs e)
        {
            try
            {
                var serialNumber = textBoxSerialNumber.Text;

                serialPortWand.Write($"z{serialNumber}");

                Command_F();
            }
            catch (Exception ex)
            { writeToScreen(ex.ToString()); }
        }

        private IEnumerable<string> ReadBackFromPort(int sleeptime = 100)
        {
            Thread.Sleep(sleeptime);
            var readback = serialPortWand.ReadExisting().Split('\n').Where(u => !String.IsNullOrEmpty(u));

            writeToScreen(readback);

            return readback;
        }

        private void writeToScreen(string readback)
        {
            listBoxReadBack.Items.Add(readback.Replace("\t", " "));
            int visibleItems = listBoxReadBack.ClientSize.Height / listBoxReadBack.ItemHeight;
            listBoxReadBack.TopIndex = Math.Max(listBoxReadBack.Items.Count - visibleItems + 1, 0);
        }

        private void writeToScreen(IEnumerable<string> readback)
        {
            listBoxReadBack.Items.AddRange(readback.Select(u => u.Replace("\t", " ")).ToArray());
            int visibleItems = listBoxReadBack.ClientSize.Height / listBoxReadBack.ItemHeight;
            listBoxReadBack.TopIndex = Math.Max(listBoxReadBack.Items.Count - visibleItems + 1, 0);
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }



        bool continueRead = false;

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            { buttonCheck.Enabled = TareWasDone; }
            catch (Exception ex)
            {
                writeToScreen(ex.ToString());
            }

            try
            {
                tabControl1.Enabled = serialPortWand.IsOpen;

                if (serialPortWand.IsOpen)
                {
                    if (continueRead)
                    { Command_I(); }
                }
                else
                {
                    continueRead = false;
                }
            }
            catch (Exception ex)
            {
                continueRead = false;
                tabControl1.Enabled = false;
                writeToScreen(ex.ToString());
            }

            if (continueRead)
            { buttonContinuousRead.BackColor = Color.Red; }
            else
            { buttonContinuousRead.BackColor = Color.Yellow; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var serialNumber = "SNTEST";
                serialPortWand.Write($"z{serialNumber}");
                Command_F();
            }
            catch (Exception ex)
            { writeToScreen(ex.ToString()); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            continueRead ^= true;
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                // user typed something
                if (e.KeyCode == Keys.Enter)
                {
                    // capture the command
                    var command = textBoxCommandLine.Text;

                    // user typed enter
                    textBoxCommandLine.Text = "";

                    if (serialPortWand.IsOpen)
                    {
                        serialPortWand.Write(command);
                        var identity = ReadBackFromPort();
                    }
                }
            }
            catch (Exception ex)
            { writeToScreen(ex.ToString()); }


        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void exportLogToFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                sfd.Title = "Browse file location";
                sfd.DefaultExt = "txt";

                var uid = textBoxUID.Text;

                sfd.FileName = $"Log{(String.IsNullOrEmpty(uid) ? "" : $"[{uid}]")}({DateTime.Now.ToString("yyyyMMdd - HHmmss")}).txt";
                var result = sfd.ShowDialog();
                if (result == DialogResult.OK)
                {
                    using (var sw = new StreamWriter(sfd.FileName))
                    {
                        sw.WriteLine("MOLLI Wand Production Tool (PT-002)");
                        sw.WriteLine("Version 1.0");
                        sw.WriteLine($"Test Date: {DateTime.Now.ToLongDateString()}");
                        sw.WriteLine($"Test Time: {DateTime.Now.ToLongTimeString()}");
                        sw.WriteLine($"Computer name: '{Environment.UserName}'");
                        sw.WriteLine("");

                        foreach (var item in listBoxReadBack.Items)
                        {
                            sw.WriteLine(item.ToString().Trim(new char[] { '\r', '\n' }));
                        }
                    }

                    Process.Start(sfd.FileName);
                }
            }
        }

        private void enableCommandLineToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {

        }

        private void enableCommandLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxCommandLine.Enabled = enableCommandLineToolStripMenuItem.Checked;
        }


        ClsTests Tests { get; set; } = new ClsTests();
        ClsSensorReading TareValue { get; set; } = new ClsSensorReading();

        bool TareWasDone = false;

        private void buttonTare_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Please remove all magnets before proceeding");

            int Average = Tests.NumOfReadingsForAverage;

            writeToScreen($"Taring Sensors, {Average} readings");

            TareValue = ClsSensorReading.GetAverage(Average);

            writeToScreen($"Tare value is {TareValue.ToString()}");

            TareWasDone = true;
        }


        private void buttonCheck_Click(object sender, EventArgs e)
        {
            // XML: please put in slot ""
            var index = comboBoxTest.SelectedIndex;

            // find out which test we are doing
            var thisTest = Tests.Positions[index];

            writeToScreen($"Running test '{thisTest.PullDownMessage}'");

            // show the user the message of this test
            MessageBox.Show(thisTest.MessageToUser);

            // get readings - average
            var readings = ClsSensorReading.GetAverage(Tests.NumOfReadingsForAverage);

            // subtract tare
            readings.Subtract(TareValue);

            // log the average value
            writeToScreen($"Average reading is {readings.ToString()}");
            writeToScreen($"Testing criteria is {thisTest.ToString()}");

            // compare that with limits in xml file
            var failedSensorIndex = readings.CheckAllSensors(thisTest.Limits);

            // show the user the results of the test
            if (failedSensorIndex == -1)
            {
                writeToScreen($"All sensors pass");
                MessageBox.Show("All sensors are within test limits");
            }
            else
            {
                writeToScreen($"Sensor {failedSensorIndex + 1} failed");
                MessageBox.Show($"Sensor #{failedSensorIndex + 1} failed the test");
            }
        }

        private void buttonStore_Click(object sender, EventArgs e)
        {

        }
    }
}
